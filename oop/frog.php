<?php

class Frog extends Animal
{
  public function __construct($pName){
    $this->name = $pName;
    $this->legs = 4;
  }

  public function jump()
  {
    echo "hop hop <br>";
  }
}
?>