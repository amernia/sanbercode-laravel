<?php
require 'animal.php';
require 'frog.php';
require 'ape.php';

    $sheep = new Animal("shaun");

    echo "name : $sheep->name <br>"; // "shaun"
    echo  "legs : $sheep->legs <br>"; // 2
    echo  "cold_blooded : $sheep->cold_blooded <br><br>"; // false 

    $sungokong = new Ape("kera sakti");
    echo "name : $sungokong->name <br>";
    echo "legs : $sungokong->legs <br>";
    echo "yell : ";
    $sungokong->yell(); // "Auooo"
    echo "<br>";
   
    $kodok = new Frog("buduk");
    echo "name : $kodok->name <br>";
    echo "legs : $kodok->legs <br>";
    echo "yell : ";
    $kodok->jump() ; // "hop hop"
?>