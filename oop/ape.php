<?php

class Ape extends Animal
{
  public function __construct($pName){
    $this->name = $pName;
    $this->legs = 2;
  }

  public function yell()
  {
    echo "Auooo <br>";
  }
}
?>