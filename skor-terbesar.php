<?php
function skor_terbesar($arr){
//kode di sini
  $hasil =[];
  $maxLaravel = 0;
  $maxReactNatve = 0;
  $maxReactJS = 0;

  foreach($arr as $key => $value){
    if($value['nilai'] > $maxLaravel && $value['kelas'] == 'Laravel'){
      $maxLaravel = $value['nilai'];
      $hasil[$value['kelas']]=[
        'nama' => $value['nama'],
        'kelas' => $value['kelas'],
        'nilai' => $value['nilai']
      ];
    }else if($value['nilai'] > $maxReactNatve && $value['kelas'] == 'React Native'){
      $maxReactNatve = $value['nilai'];
      $hasil[$value['kelas']]=[
        'nama' => $value['nama'],
        'kelas' => $value['kelas'],
        'nilai' => $value['nilai']
      ];
    }else if($value['nilai'] > $maxReactJS && $value['kelas'] == 'React JS'){
      $maxReactJS = $value['nilai'];
      $hasil[$value['kelas']]=[
        'nama' => $value['nama'],
        'kelas' => $value['kelas'],
        'nilai' => $value['nilai']
      ];
    }
  }

 
  return $hasil;
}

// TEST CASES
$skor = [
  [
    "nama" => "Bobby",
    "kelas" => "Laravel",
    "nilai" => 78
  ],
  [
    "nama" => "Regi",
    "kelas" => "React Native",
    "nilai" => 86
  ],
  [
    "nama" => "Aghnat",
    "kelas" => "Laravel",
    "nilai" => 90
  ],
  [
    "nama" => "Indra",
    "kelas" => "React JS",
    "nilai" => 85
  ],
  [
    "nama" => "Yoga",
    "kelas" => "React Native",
    "nilai" => 77
  ],
];
echo "<pre>";
print_r(skor_terbesar($skor));
echo "</pre>";
/* OUTPUT
  Array (
    [Laravel] => Array
              (
                [nama] => Aghnat
                [kelas] => Laravel
                [nilai] => 90
              )
    [React Native] => Array
                  (
                    [nama] => Regi
                    [kelas] => React Native
                    [nilai] => 86
                  )
    [React JS] => Array
                (
                  [nama] => Indra
                  [kelas] => React JS
                  [nilai] => 85
                )
  )
*/
?>