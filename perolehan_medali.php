<?php
    function perolehan_medali($arr){
        //pseudocode
        //1. declare var penampung untuk hasil, jumlah emas, perak, perunggu masing-masing negara
        //2. pengecekkan jika array kosong atau tidak
        //3. looping sebanyak nilai array yang ada
        //4. pengecekkan tiap negara
        //5. pengecekkan tiap medali
        //6. tiap medali ditambahkan satu sesuai dengan jenis medali
        //7. nama negara dan jumlah tiap medali dimasukkan kedalam var array penampung hasil
        //8. return var penampung
        
        $hasil = [];
        $emasIndo = 0;
        $perakIndo = 0;
        $perungIndo = 0;

        $emasIndia = 0;
        $perakIndia = 0;
        $perungIndia= 0;

        $emasKorsel = 0;
        $perakKorsel = 0;
        $perungKorsel = 0;

        if ($arr != []){
            foreach($arr as $key=>$value){
                if($value[0] == 'Indonesia'){
                    if($value[1] == 'emas'){
                        $emasIndo ++;
                    }else if($value[1] == 'perak'){
                        $perakIndo ++;
                    }
    
                    $hasil[$value[0]] = [
                        'negara' => $value[0],
                        'emas' => $emasIndo,
                        'perak' => $perakIndo,
                        'perunggu' => $perungIndo
                    ];
                }else if($value[0] == 'India'){
                    if($value[1] == 'emas'){
                        $emasIndia ++;
                    }else if($value[1] == 'perak'){
                        $perakIndia ++;
                    }
    
                    $hasil[$value[0]] = [
                        'negara' => $value[0],
                        'emas' => $emasIndia,
                        'perak' => $perakIndia,
                        'perunggu' => $perungIndia
                    ];
    
                }else if($value[0] == 'Korea Selatan'){
                    if($value[1] == 'emas'){
                        $emasKorsel ++;
                    }else if($value[1] == 'perak'){
                        $perakKorsel ++;
                    }
    
                    $hasil[$value[0]] = [
                        'negara' => $value[0],
                        'emas' => $emasKorsel,
                        'perak' => $perakKorsel,
                        'perunggu' => $perungKorsel
                    ];
                }
            }
            return $hasil;
        }else{
            echo 'no data';
        }  
    }
    echo "<pre>";
    print_r (perolehan_medali(
        array(
            array('Indonesia','emas'),
            array('India','perak'),
            array('Korea Selatan','emas'),
            array('India','perak'),
            array('India','emas'),
            array('Indonesia','perak'),
            array('Indonesia','emas'),
        )
    ));
    echo "</pre>";

    // print_r (perolehan_medali([]));
?>