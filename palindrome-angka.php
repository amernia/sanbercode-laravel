<?php

function palindrome($string){
  $output = false;
  if (strrev($string) == $string){
    $output = true;
  }
  return $output;
}

function palindrome_angka($angka) {
  // tulis kode di sini
  $hasil = 0;
  echo "angka : $angka ";
  if($angka >= 1 && $angka <= 8){
    $hasil = $angka++;
  }else{
    while (palindrome($angka) == false){
      $hasil = $angka++;
    }
    $hasil = $angka;
  }
  if(palindrome($angka)){
    $hasil = $angka++;
  }
  echo " = $hasil <br>";
}

// TEST CASES
echo palindrome_angka(8); // 9
echo palindrome_angka(10); // 11
echo palindrome_angka(117); // 121
echo palindrome_angka(175); // 181
echo palindrome_angka(1000); // 1001

?>