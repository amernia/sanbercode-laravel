@extends('template.utama')
@section('isi')

<div class="container">
    <h3>Input Pertanyaan</h3>
    <form class="mt-2" action='{{url("pertanyaan")}}' method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="form-group">
            <label for="judul">Judul : </label>
            <div class="row">
                <div class="col-md-3">
                    <input type="text" class="form-control" name="judul" id="judul">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="isi">Isi : </label>
            <div class="row">
                <div class="col-md-3">
                    <input type="text" class="form-control" name="isi" id="isi">
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
@endsection