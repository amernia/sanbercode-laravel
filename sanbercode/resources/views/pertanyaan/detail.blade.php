@extends('template.utama')
@section('isi')
<div class="container">
    <label>Judul : {{$data->judul}}</label><br>
    <label>Isi : {{$data->isi}}</label><br>
    <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
        <a class="btn btn-default" href="{{ route('pertanyaan.index') }}">Kembali</a>
      </div>
    </div>
</div>
@endsection