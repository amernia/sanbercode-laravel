@extends('template.utama')
@section('isi')

<div class="container">
  @foreach($errors->all() as $error)      
    <h4>{{$error}}</h4>
  @endforeach
  @if (session('status'))
    <div>{{session('status')}}</div>
  @endif
    <h3>Data Pertanyaan</h3>
    <a class="btn btn-info" href="{{ url('pertanyaan/create') }}">Tambah Baru</a>
    <table class="table table-hover">
        <thead>
        <tr>
            <th scope="col">No</th>
            <th scope="col">Pertanyaan</th>
            <th scope="col">Jawaban</th>
        </tr>
        </thead>
        <tbody>
        @foreach($pertanyaan as $idx => $p)
        <tr>
            <td><a href="{{ route('pertanyaan.show', $p->id) }}">{{$idx+1}}</a></td>
            <td><a href="{{ route('pertanyaan.show', $p->id) }}">{{$p->isi}}</a></td>
            <td>
            <a class="btn btn-warning" href="{{ route('pertanyaan.edit', $p->id) }}">Edit</a>
            <form action="{{ route('pertanyaan.destroy', $p->id) }}" method="POST" id="form-hapus-{{ $p->id }}">
                {{ method_field("DELETE") }}
                {{ csrf_field() }}
                <button type="submit" class="btn btn-danger">Delete</button>
            </form>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
</div>
@endsection