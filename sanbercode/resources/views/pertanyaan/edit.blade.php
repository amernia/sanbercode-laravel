@extends('template.utama')
@section('isi')
<div class="container">
<h2>Ubah Pertanyaan</h2>
  <form class="form-horizontal" action="{{ route('pertanyaan.update', $data->id) }}" method="post">
  <!-- semua method post wajib ada csrf -->
  {!! csrf_field() !!}
  <!-- mengganti method post ke put -->
  {{ method_field('PUT') }}
    <div class="form-group">
            <label for="judul">Judul : </label>
            <div class="row">
                <div class="col-md-3">
                    <input type="text" class="form-control" name="judul" id="judul" value="{{ $data->judul }}">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="isi">Isi : </label>
            <div class="row">
                <div class="col-md-3">
                    <input type="text" class="form-control" name="isi" id="isi" value="{{ $data->isi }}">
                </div>
            </div>
        </div>
    <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-default">Submit</button>
        <a class="btn btn-default" href="{{ route('pertanyaan.index') }}">Kembali</a>
      </div>
    </div>
  </form>
</div>
@endsection