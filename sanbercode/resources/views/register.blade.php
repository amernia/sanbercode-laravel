@extends('template.utama')
@section('isi')

<h2>Buat Account Baru!</h2>
<h3>Sign Up Form</h3>
<form method = "POST" action="{{url('welcome')}}">
    {{csrf_field()}}
    <label for="firstName" id = "firstName">First Name:</label><br>
    <input type="text"  name="firstName"><br><br>
    <label for="lastName" id="lastName">Last Name:</label><br>
    <input type="text"  name="lastName"><br><br>

    <label for="gender">Gender:</label><br>
    <input type="radio" id="male" name="gender">
    <label for="male">Male</label><br>
    <input type="radio" id="female" name="gender">
    <label for="female">Female</label><br>
    <input type="radio" id="other" name="gender">
    <label for="other">Other</label><br><br>

    <label for="nationality">Nationality:</label><br>
    <select name="nationality" id="nationality">
        <option value="indo">Indonesian</option>
        <option value="malay">Malaysian</option>
        <option value="spore">Singapore</option>
        <option value="aust">Australia</option>
    </select><br><br>

    <label for="language">Language Spoken:</label><br>
        <input type="checkbox" id="indo" name="language">
        <label for="indo">Bahasa Indonesia</label><br>
        <input type="checkbox" id="eng" name="language">
        <label for="eng">English</label><br>
        <input type="checkbox" id="other" name="language">
        <label for="other">Other</label><br><br>

    <label for="bio">Bio:</label><br>
    <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br>
    <input type="submit" id="signUp" value="Sign Up">
</form>
@endsection