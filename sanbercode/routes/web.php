<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('/','HomeController');
Route::get('/register','AuthController@create');
Route::post('/welcome','AuthController@store');

Route::get('/table', function () {
    return view ('table');
});

Route::get('/data-tables', function () {
    return view ('dataTable');
});

Route::resource('/pertanyaan','PertanyaanController');