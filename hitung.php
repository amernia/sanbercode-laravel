<?php
    function hitung($string_data){
        //pseudocode
        //1. declare var penampung
        //2. looping sebanyak panjang string
        //3. pengecekkan string ke i termasuk operator yang mana
        //4. diambil angka per angka menggunakan substring
        //5. dihitung sesuai operator yang didapatkan
        //6. return hasil

        $hasil = 0;
        for($i = 0; $i <= strlen($string_data); $i++){
            if($string_data[$i] == '*'){
                $hasil = substr($string_data,0,3) * substr($string_data,4);
            }else if($string_data[$i] == '+'){
                $hasil = substr($string_data,0,1) + substr($string_data,2);
            }
            else if($string_data[$i] == ':'){
                $hasil = substr($string_data,0,3) / substr($string_data,4,5);
            }
            else if($string_data[$i] == '%'){
                $hasil = substr($string_data,0,2) % substr($string_data,3);
            }else if($string_data[$i] == '-'){
                $hasil = substr($string_data,0,2) - substr($string_data,3);
            }
        }
        return $hasil."<br>" ;
    }

    echo hitung("102*2");
    echo hitung("2+3");
    echo hitung("100:25");
    echo hitung("10%2");
    echo hitung("99-2");
?>
